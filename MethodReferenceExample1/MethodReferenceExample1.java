public class MethodReferenceExample1 {
    public static void main(String[] args) throws InterruptedException {
        // sample example of Method reference in lambda expression
        Thread thread = new Thread(() -> printMessge());

        // Updated logic of method reference in lambda expression
        Thread thread2 = new Thread(MethodReferenceExample1::printMessge);

        thread.start();
        thread2.start();

    }

    public static void printMessge() {
        for (int i = 0; i < 20; i++) {
            System.out.println("Hi sajal");
        }
    }
}