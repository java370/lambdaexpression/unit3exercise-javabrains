import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MethodReferenceExample2 {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carroll", 42),
                new Person("Thomas", "Carlyle", 51),
                new Person("Charlotte", "Bronte", 45),
                new Person("Matthew", "Arnold", 39));

        /**
         * System is a Class
         * out is a Variable
         * println() is a method
         */

        conditionalPrinting(people, p -> true, System.out::println);
        // it will automatically get the value to print
        conditionalPrinting(people, p -> true, System.out::print);

    }

    // implemented predicate interface to avoid Condition interface implemntation
    private static void conditionalPrinting(List<Person> people, Predicate<Person> predicate,
            Consumer<Person> consumer) {
        for (Person person : people) {
            if (predicate.test(person)) {
                consumer.accept(person);
            }
        }
    }
}